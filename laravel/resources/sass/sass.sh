
MAIN_SASS_FILE="app.scss"
UTILS_FOLDER="utils"
BASE_FOLDER="base"
COMPONENTS_FOLDER="components"
PARTIALS_FOLDER="partials"
PAGES_FOLDER="pages"

COLOR_BLUE=$'\e[34m'
COLOR_RED=$'\e[31m'
RESET_TEXT=$'\e[0m'

####################################
# Create app.scss if doesn't exist #
####################################
if [ ! -f "$MAIN_SASS_FILE" ]
then
	touch app.scss
	echo -e "File $COLOR_BLUE app.scss $RESET_TEXT created successfully"
fi

#################################
# Create and fill UTILS folders #
#################################
if [ ! -d "$UTILS_FOLDER" ]
then
    mkdir "$UTILS_FOLDER"
    echo -e "Directory > $COLOR_RED $UTILS_FOLDER $RESET_TEXT created successfully"

    touch "$UTILS_FOLDER"/_variables.scss
    echo -e ". . . File $COLOR_BLUE _variables.scss $RESET_TEXT created successfully"

    touch "$UTILS_FOLDER"/_mixins.scss
    echo -e ". . . File $COLOR_BLUE _mixins.scss $RESET_TEXT created successfully"
fi

################################
# Create and fill BASE folders #
################################
if [ ! -d "$BASE_FOLDER" ]
then
    mkdir "$BASE_FOLDER"
    echo -e "Directory > $COLOR_RED $BASE_FOLDER $RESET_TEXT created successfully"

    touch "$BASE_FOLDER"/_reset.scss
    echo -e ". . . File $COLOR_BLUE _reset.scss $RESET_TEXT created successfully"

    touch "$BASE_FOLDER"/_typography.scss
    echo -e ". . . File $COLOR_BLUE _typography.scss $RESET_TEXT created successfully"
fi

######################################
# Create and fill COMPONENTS folders #
######################################
if [ ! -d "$COMPONENTS_FOLDER" ]
then
    mkdir "$COMPONENTS_FOLDER"
    echo -e "Directory > $COLOR_RED $COMPONENTS_FOLDER $RESET_TEXT created successfully"

    touch "$COMPONENTS_FOLDER"/_buttons.scss
    echo -e ". . . File $COLOR_BLUE _buttons.scss $RESET_TEXT created successfully"
fi

####################################
# Create and fill PARTIALS folders #
####################################
if [ ! -d "$PARTIALS_FOLDER" ]
then
    mkdir "$PARTIALS_FOLDER"
    echo -e "Directory > $COLOR_RED $PARTIALS_FOLDER $RESET_TEXT created successfully"

    touch "$PARTIALS_FOLDER"/_header.scss
    echo -e ". . . File $COLOR_BLUE _header.scss $RESET_TEXT created successfully"

    touch "$PARTIALS_FOLDER"/_footer.scss
    echo -e ". . . File $COLOR_BLUE _footer.scss $RESET_TEXT created successfully"

    touch "$PARTIALS_FOLDER"/_forms.scss
    echo -e ". . . File $COLOR_BLUE _forms.scss $RESET_TEXT created successfully"
fi

#################################
# Create and fill PAGES folders #
#################################
if [ ! -d "$PAGES_FOLDER" ]
then
    mkdir "$PAGES_FOLDER"
    echo -e "Directory > $COLOR_RED $PAGES_FOLDER $RESET_TEXT created successfully"

    touch "$PAGES_FOLDER"/_home.scss
    echo -e ". . . File $COLOR_BLUE _home.scss $RESET_TEXT created successfully"
fi

##############################
# Import all dep in app.scss #
##############################
cat >$MAIN_SASS_FILE <<EOL
// UTILS
@import "$UTILS_FOLDER/variables";
@import "$UTILS_FOLDER/mixins";

// BASE
@import "$BASE_FOLDER/reset";
@import "$BASE_FOLDER/typography";

// COMPONENTS
@import "$COMPONENTS_FOLDER/buttons";

// PARTIALS
@import "$PARTIALS_FOLDER/header";
@import "$PARTIALS_FOLDER/footer";
@import "$PARTIALS_FOLDER/forms";

// PAGES
@import "$PAGES_FOLDER/home";

EOL

# Fill _reset.scss
# https://gist.githubusercontent.com/hcatlin/1027867/raw/dc4f0c2ebf8e58026e65edc9fbabe812c3621805/reset.scss
cat >$BASE_FOLDER/_reset.scss <<EOL
/* http://meyerweb.com/eric/tools/css/reset/
   v2.0 | 20110126
   License: none (public domain)
*/

html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
  margin: 0;
  padding: 0;
  border: 0;
  font-size: 100%;
  font: inherit;
  vertical-align: baseline; }

/* HTML5 display-role reset for older browsers */

article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section {
  display: block; }

body {
  line-height: 1; }

ol, ul {
  list-style: none; }

blockquote, q {
  quotes: none; }

blockquote {
  &:before, &:after {
    content: '';
    content: none; } }

q {
  &:before, &:after {
    content: '';
    content: none; } }

table {
  border-collapse: collapse;
  border-spacing: 0; }
EOL
